#include "MyTgBot.h"

#define ZERO_COPY(STR)    ((char*)STR.c_str())
#define BOT_CMD(STR)      buildCommand(F(STR))

MyTgBot::MyTgBot(const String& token)
{
  this->_token  = token;
}

void MyTgBot::setClient(WiFiClientSecure &client)
{
    this->_client = &client;
    #ifdef TELEGRAM_DEBUG
    Serial.println(F("Set client"));
    #endif
}

String MyTgBot::getToken()
{
  return _token;
}

String MyTgBot::buildCommand(const String& cmd)
{
  String command;
  command += F("bot");
  command += _token;
  command += F("/");
  command += cmd;
  return command;
}

bool MyTgBot::readHTTPAnswer(String &body, String &headers)
{
  int ch_count = 0;
  unsigned long now = millis();

  bool finishedHeaders    = false;
  bool responseReceived   = false;
  bool currentLineIsBlank = true;

  while (millis() - now < longPoll * 1000 + waitForResponse) {
    while (_client->available()) {
      char c = _client->read();
      responseReceived = true;

      if (!finishedHeaders) {
        if (currentLineIsBlank && c == '\n') {
          finishedHeaders = true;
        } else {
          headers += c;
        }
      } else {
        if (ch_count < maxMessageLength) {
          body += c;
          ch_count++;
        }
      }

      if (c == '\n') currentLineIsBlank = true;
      else if (c != '\r') currentLineIsBlank = false;
    }

    if (responseReceived) {
      #ifdef TELEGRAM_DEBUG
      Serial.println();
      Serial.println(body);
      Serial.println();
      #endif
      break;
    }
  }
  return responseReceived;
}

String MyTgBot::sendGetToTelegram(const String& command)
{

  String body;
  String headers;

  if(_client->connected())
  {
    _client->print(F("GET /"));
    _client->print(command);
    _client->println(F(" HTTP/1.1"));
    _client->println(F("Host:" TELEGRAM_HOST));
    _client->println(F("Accept: application/json"));
    _client->println(F("Cache-Control: no-cache"));
    _client->println();
    #ifdef TELEGRAM_DEBUG
    Serial.println("Sending: " + command);
    #endif
    readHTTPAnswer(body, headers);
  }
  return body;
}


String MyTgBot::sendPostToTelegram(const String& command, JsonObject payload)
{

  String out;
  String body;
  String headers;

  if (_client->connected()) {
    _client->print(F("POST /"));
    _client->print(command);
    _client->println(F(" HTTP/1.1"));
    _client->println(F("HoSt:" TELEGRAM_HOST));
    _client->println(F("Content-Type: application/json"));
    int length = measureJson(payload);
    _client->print(F("Content-Length:"));
    _client->println(length);
    _client->println();
    serializeJson(payload, out);
    _client->println(out);
    #ifdef TELEGRAM_DEBUG
    Serial.println(String("Posting:") + out);
    #endif
    readHTTPAnswer(body, headers);
  }
  return body;
}

int MyTgBot::getUpdates(long offset)
{
  #ifdef TELEGRAM_DEBUG
  Serial.println(F("GET Update Messages"));
  #endif
  String cmd = BOT_CMD("getUpdates?offset=");
  cmd += offset;
  cmd += F("&limit=1");

  String response = sendGetToTelegram(cmd); // receive reply from telegram.org

  #ifdef TELEGRAM_DEBUG
  Serial.print(F("Incoming message length: "));
  Serial.println(response.length());
  Serial.println(F("Creating DynamicJsonBuffer"));
  #endif

  if(response.length() < 23)
  {
    #ifdef TELEGRAM_DEBUG
    Serial.println("Small Message");
    #endif
    return 1;
  }

  DynamicJsonDocument doc(maxMessageLength);
  DeserializationError error = deserializeJson(doc, ZERO_COPY(response));

  if (!error) {
    #ifdef TELEGRAM_DEBUG
    Serial.print(F("GetUpdates parsed jsonObj: "));
    serializeJson(doc, Serial);
    Serial.println();
    #endif
    if (doc.containsKey("result")) {
        int resultArrayLength = doc["result"].size();
        if (resultArrayLength > 0) {
          int newMessageIndex = 0;
          // Step through all results
          for (int i = 0; i < resultArrayLength; i++) {
            JsonObject result = doc["result"][i];
            if (processResult(result, newMessageIndex)) newMessageIndex++;
          }
          // We will keep the client open because there may be a response to be
          // given
          return newMessageIndex;
        } else {
          #ifdef TELEGRAM_DEBUG
            Serial.println(F("no new messages"));
          #endif
        }
    #ifdef TELEGRAM_DEBUG
    } else {
        Serial.println(F("Response contained no 'result'"));
    #endif
    }
  } else { // Parsing failed
    if(response.length() < 2)
    { // Too short a message. Maybe a connection issue
      #ifdef TELEGRAM_DEBUG
        Serial.println(F("Parsing error: Message too short"));
      #endif
    } else {
      // Buffer may not be big enough, increase buffer or reduce max number of
      // messages
      #ifdef TELEGRAM_DEBUG
        Serial.print(F("Failed to parse update, the message could be too "
                       "big for the buffer. Error code: "));
        Serial.println(error.c_str()); // debug print of parsing error
      #endif
    }
  }
  return 0;
}

bool MyTgBot::getMe()
{
  String response = sendGetToTelegram(BOT_CMD("getMe")); // receive reply from telegram.org
  DynamicJsonDocument doc(maxMessageLength);
  DeserializationError error = deserializeJson(doc, ZERO_COPY(response));
  if(!error)
  {
    if(doc.containsKey("result"))
    {
      name = doc["result"]["first_name"].as<String>();
      userName = doc["result"]["username"].as<String>();
      return true;
    }
  }
  return false;
}

bool MyTgBot::processResult(JsonObject result, int messageIndex)
{
  int update_id = result["update_id"];
  // Check have we already dealt with this message (this shouldn't happen!)
  if (last_message_received != update_id) {
    last_message_received = update_id;
    messages[messageIndex].update_id = update_id;
    messages[messageIndex].text = F("");
    messages[messageIndex].from_id = F("");
    messages[messageIndex].from_name = F("");
    messages[messageIndex].longitude = 0;
    messages[messageIndex].latitude = 0;
    messages[messageIndex].reply_to_message_id = 0;
    messages[messageIndex].reply_to_text = F("");
    messages[messageIndex].query_id = F("");

    if (result.containsKey("message")) {
      JsonObject message = result["message"];
      messages[messageIndex].type = F("message");
      messages[messageIndex].from_id = message["from"]["id"].as<String>();
      messages[messageIndex].from_name = message["from"]["first_name"].as<String>();
      messages[messageIndex].date = message["date"].as<String>();
      messages[messageIndex].chat_id = message["chat"]["id"].as<String>();
      messages[messageIndex].chat_title = message["chat"]["title"].as<String>();
      messages[messageIndex].hasDocument = false;
      messages[messageIndex].message_id = message["message_id"].as<int>();  // added message id
      if (message.containsKey("text")) {
        messages[messageIndex].text = message["text"].as<String>();
      } else if (message.containsKey("location")) {
        messages[messageIndex].longitude = message["location"]["longitude"].as<float>();
        messages[messageIndex].latitude  = message["location"]["latitude"].as<float>();
      } else if (message.containsKey("document")) {
        String file_id = message["document"]["file_id"].as<String>();
        messages[messageIndex].file_caption = message["caption"].as<String>();
        messages[messageIndex].file_name = message["document"]["file_name"].as<String>();
        if (getFile(messages[messageIndex].file_path, messages[messageIndex].file_size, file_id) == true)
          messages[messageIndex].hasDocument = true;
        else
          messages[messageIndex].hasDocument = false;
      }
      if (message.containsKey("reply_to_message")) {
        messages[messageIndex].reply_to_message_id = message["reply_to_message"]["message_id"];
        // no need to check if containsKey["text"]. If it doesn't, it default to null
        messages[messageIndex].reply_to_text = message["reply_to_message"]["text"].as<String>();
      }

    } else if (result.containsKey("channel_post")) {
      JsonObject message = result["channel_post"];
      messages[messageIndex].type = F("channel_post");
      messages[messageIndex].text = message["text"].as<String>();
      messages[messageIndex].date = message["date"].as<String>();
      messages[messageIndex].chat_id = message["chat"]["id"].as<String>();
      messages[messageIndex].chat_title = message["chat"]["title"].as<String>();
      messages[messageIndex].message_id = message["message_id"].as<int>();  // added message id

    } else if (result.containsKey("callback_query")) {
      JsonObject message = result["callback_query"];
      messages[messageIndex].type = F("callback_query");
      messages[messageIndex].from_id = message["from"]["id"].as<String>();
      messages[messageIndex].from_name = message["from"]["first_name"].as<String>();
      messages[messageIndex].text = message["data"].as<String>();
      messages[messageIndex].date = message["date"].as<String>();
      messages[messageIndex].chat_id = message["message"]["chat"]["id"].as<String>();
      messages[messageIndex].reply_to_text = message["message"]["text"].as<String>();
      messages[messageIndex].chat_title = F("");
      messages[messageIndex].query_id = message["id"].as<String>();
      messages[messageIndex].message_id = message["message"]["message_id"].as<int>();  // added message id

    } else if (result.containsKey("edited_message")) {
      JsonObject message = result["edited_message"];
      messages[messageIndex].type = F("edited_message");
      messages[messageIndex].from_id = message["from"]["id"].as<String>();
      messages[messageIndex].from_name = message["from"]["first_name"].as<String>();
      messages[messageIndex].date = message["date"].as<String>();
      messages[messageIndex].chat_id = message["chat"]["id"].as<String>();
      messages[messageIndex].chat_title = message["chat"]["title"].as<String>();
      messages[messageIndex].message_id = message["message_id"].as<int>();  // added message id

      if (message.containsKey("text")) {
        messages[messageIndex].text = message["text"].as<String>();
      } else if (message.containsKey("location")) {
        messages[messageIndex].longitude = message["location"]["longitude"].as<float>();
        messages[messageIndex].latitude  = message["location"]["latitude"].as<float>();
      }
    }
    return true;
  }
  return false;
}

bool MyTgBot::getFile(String& file_path, long& file_size, const String& file_id)
{
  String command = BOT_CMD("getFile?file_id=");
  command += file_id;
  String response = sendGetToTelegram(command); // receive reply from telegram.org
  DynamicJsonDocument doc(maxMessageLength);
  DeserializationError error = deserializeJson(doc, ZERO_COPY(response));

  if (!error)
  {
    if (doc.containsKey("result"))
    {
      file_path  = F("https://api.telegram.org/file/");
      file_path += buildCommand(doc["result"]["file_path"]);
      file_size = doc["result"]["file_size"].as<long>();
      return true;
    }
  }
  return false;
}

bool MyTgBot::checkForOkResponse(const String& response)
{
  int last_id;
  DynamicJsonDocument doc(maxMessageLength);
  deserializeJson(doc, response);
  // Save last sent message_id
  last_id = doc["result"]["message_id"];
  if (last_id > 0) last_sent_message_id = last_id;
  return doc["ok"] | false;  // default is false, but this is more explicit and clear
}

bool MyTgBot::sendSimpleMessage(const String& chat_id, const String& text, const String& parse_mode)
{
  bool sent = false;
  #ifdef TELEGRAM_DEBUG
  Serial.println(F("sendSimpleMessage: SEND Simple Message"));
  #endif
  unsigned long sttime = millis();
  if (text != "") {
    while (millis() - sttime < 8000ul) { // loop for a while to send the message
      String command = BOT_CMD("sendMessage?chat_id=");
      command += chat_id;
      command += F("&text=");
      command += text;
      command += F("&parse_mode=");
      command += parse_mode;
      String response = sendGetToTelegram(command);
      #ifdef TELEGRAM_DEBUG
      Serial.println(response);
      #endif
      sent = checkForOkResponse(response);
      if(sent) break;
    }
  }
  return sent;
}

