#ifndef MyTgBot_h
#define MyTgBot_h

#include <Arduino.h>
#define ARDUINOJSON_DECODE_UNICODE 1
#define ARDUINOJSON_USE_LONG_LONG 1
#include <ArduinoJson.h>
#include <MyTgCert.h>
#include <WiFiClientSecure.h>

//#define TELEGRAM_DEBUG    1
#define TELEGRAM_HOST     "api.telegram.org"
#define TELEGRAM_SSL_PORT 443

struct telegramMessage {
  String text;
  String chat_id;
  String chat_title;
  String from_id;
  String from_name;
  String date;
  String type;
  String file_caption;
  String file_path;
  String file_name;
  bool hasDocument;
  long file_size;
  float longitude;
  float latitude;
  int update_id;
  int message_id;
  int reply_to_message_id;
  String reply_to_text;
  String query_id;
};

class MyTgBot
{

public:
    int longPoll = 0;
    String name;
    String userName;
    int maxMessageLength = 1500;
    unsigned int waitForResponse = 1500;
    int last_sent_message_id = 0;
    long last_message_received = 0;
    telegramMessage messages[2];

    MyTgBot(const String& token);

    String getToken();

    bool getMe();
    bool getFile(String& file_path, long& file_size, const String& file_id);
    bool sendSimpleMessage(const String& chat_id, const String& text, const String& parse_mode);

    String buildCommand(const String& cmd);

    bool readHTTPAnswer(String &body, String &headers);

    String sendGetToTelegram(const String& command);

    String sendPostToTelegram(const String& command, JsonObject payload);

    int getUpdates(long offset);

    void setClient(WiFiClientSecure& client);


private:

    String  _token;
    Client* _client;

    bool processResult(JsonObject result, int messageIndex);
    bool checkForOkResponse(const String& response);

};

#endif
