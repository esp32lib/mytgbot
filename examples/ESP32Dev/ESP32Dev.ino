#include <WiFi.h>

const char* ssid = "xxxx";
const char* password = "xxxxx";

#include <WiFiClient.h>
#include <WiFiClientSecure.h>

#include <MyTgBot.h>

#define BOTtoken "2116497751:AAESKvQkzx2pbKWKKml3-WyxkSKJljDAaOc"
#define CHAT_ID  "-1001207108890"

MyTgBot bot(BOTtoken);

// Задаем действия при получении новых сообщений
void handleNewMessages()
{
  // Идентификатор чата запроса
  String chat_id = String(bot.messages[0].chat_id);

  // Выводим полученное сообщение
  String text = bot.messages[0].text;
  Serial.println(text);

  String from_name = bot.messages[0].from_name;

  if ((text == "/ping") || (text == "/ping@garageSecurity_bot"))
  {
    bot.sendSimpleMessage(chat_id, "PONG", "");
  }
}

void setup()
{
  // иницилизируем монитор порта
  Serial.begin(115200);

  // Подключаемся к Wi-Fi
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.print("Connecting to WiFi ");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");

  // Выводим IP ESP32
  Serial.println(WiFi.localIP());

  configTime(3 * 3600, 0, "pool.ntp.org");

  WiFiClientSecure client = WiFiClientSecure();
  client.setCACert(TELEGRAM_CERTIFICATE_ROOT);
  client.connect(TELEGRAM_HOST, TELEGRAM_SSL_PORT);
  bot.setClient(client);
  bot.sendSimpleMessage(CHAT_ID, "GarageSecurity Включен. Узнать состояние /ping", "");
  ~client;
}

int numNewMessages = 0;
unsigned long lastTime1 = 0;

void loop()
{
  if (millis() - lastTime1 > 3000)
  {
    WiFiClientSecure client = WiFiClientSecure();
    client.setCACert(TELEGRAM_CERTIFICATE_ROOT);
    client.connect(TELEGRAM_HOST, TELEGRAM_SSL_PORT);
    bot.setClient(client);

    numNewMessages = bot.getUpdates(bot.last_message_received + 1);
    while (numNewMessages) {
      Serial.println("Got response");
      handleNewMessages();
      numNewMessages = bot.getUpdates(bot.last_message_received + 1);
    }
    ~client;
    lastTime1 = millis();
  }



}
